#!/usr/bin/env python

# ###########################################################################
#
# This file is part of Taurus
#
# http://taurus-scada.org
#
# Copyright 2011 CELLS / ALBA Synchrotron, Bellaterra, Spain
#
# Taurus is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Taurus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Taurus.  If not, see <http://www.gnu.org/licenses/>.
#
# ###########################################################################

"""This package consists of a collection of useful classes and functions. Most
of the elements are taurus independent and can be used generically.
"""

__docformat__ = "restructuredtext"

# taurus cannot work properly without the following modules so
# they are promptly imported here has a facility (also for backward
# compatibility)
# However, new applications should in their code use the full import.
# Example, use:
#     from taurus.core.util.log import Logger
# instead of:
#     from taurus.core.util import Logger

from .containers import *  # noqa: F403,F401
from .enumeration import *  # noqa: F403,F401
from .event import *  # noqa: F403,F401
from .log import *  # noqa: F403,F401
from .object import *  # noqa: F403,F401
from .singleton import *  # noqa: F403,F401

# from .codecs import *
# from .colors import *
# from .constant import *
# from .timer import *
# from .safeeval import *
# from .prop import *
# from .threadpool import *
# from .user import *

# import eventfilters

# try:
# from lxml import etree
# except Exception:
# etree = None


def dictFromSequence(seq):
    """Translates a sequence into a dictionary by converting each to elements
    of the sequence (k,v) into a k:v pair in the dictionary

    :param seq: any sequence object
    :type seq: sequence
    :return: dictionary built from the given sequence
    :rtype: dict
    """

    def _pairwise(iterable):
        """Utility method used by dictFromSequence"""
        itnext = iter(iterable).__next__
        while True:
            yield itnext(), itnext()

    return dict(_pairwise(seq))
