
.. image:: https://img.shields.io/pypi/pyversions/taurus.svg
    :target: https://pypi.python.org/pypi/taurus
    :alt: Python Versions
    
.. image:: https://img.shields.io/pypi/l/taurus.svg
    :target: https://pypi.python.org/pypi/taurus
    :alt: License
    
.. image:: https://img.shields.io/pypi/v/taurus.svg
    :target: https://pypi.python.org/pypi/taurus
    :alt: Latest Version

.. image:: https://anaconda.org/conda-forge/taurus/badges/version.svg
    :target: https://anaconda.org/conda-forge/taurus
    :alt: Conda

.. image:: https://gitlab.com/taurus-org/taurus/badges/develop/pipeline.svg
    :target: https://gitlab.com/taurus-org/taurus/-/commits/develop
    :alt: Gitlab CI status



Taurus
========

Taurus is a control and data acquisition python module

Main web site: http://taurus-scada.org


