#!/usr/bin/env python

# ###########################################################################
#
# This file is part of Taurus
#
# http://taurus-scada.org
#
# Copyright 2011 CELLS / ALBA Synchrotron, Bellaterra, Spain
#
# Taurus is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Taurus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Taurus.  If not, see <http://www.gnu.org/licenses/>.
#
# ###########################################################################

"""This package contains a collection of taurus widgets designed to display
taurus information, typically in a read-only fashion (no user interaction is
possible). Examples of widgets that suite this rule are labels, leds and LCDs
"""

__docformat__ = "restructuredtext"

from .qfallback import *  # noqa: F403,F401
from .qpixmapwidget import *  # noqa: F403,F401
from .qled import *  # noqa: F403,F401
from .qlogo import *  # noqa: F403,F401
from .qsevensegment import *  # noqa: F403,F401
from .tauruslabel import *  # noqa: F403,F401
from .taurusled import *  # noqa: F403,F401
from .tauruslcd import *  # noqa: F403,F401
